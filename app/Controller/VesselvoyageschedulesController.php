<?php
class VesselvoyageschedulesController extends AppController {
    public $uses = array('Vesselvoyageschedule');
    public $helpers = array('Html', 'Form');
    public $components = array('RequestHandler');
 
 
    // get current date code      **********CURDATE()********
    public function index() {
        $vesselvoyageschedules = $this->Vesselvoyageschedule->find('all',array(
            'fields' => array('Vesselvoyageschedule.scn','Vesselvoyageschedule.ves_name','Vesselvoyageschedule.eta_date','Vesselvoyageschedule.eta_time','Vesselvoyageschedule.etb_date','Vesselvoyageschedule.etb_time','Vesselvoyageschedule.agent_name','Vesselvoyageschedule.berth_date','Vesselvoyageschedule.berth_time','Vesselvoyageschedule.depart_date','Vesselvoyageschedule.depart_time','Vesselvoyageschedule.first_lift_date','Vesselvoyageschedule.first_lift_time'),
            'conditions' => array(
                
            ),'order' => array('Vesselvoyageschedule.eta_date DESC')));
        $this->set(array(
            'vesselvoyageschedules' => $vesselvoyageschedules,
            '_serialize' => array('vesselvoyageschedules')
        ));
    }
 
    public function add() {
        $this->Vesselvoyageschedule->create();
        if ($this->Vesselvoyageschedule->save($this->request->data)) {
             $message = 'Created';
        } else {
            $message = 'Error';
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
    }
     
    public function view($id) {
        $vesselvoyageschedule = $this->Vesselvoyageschedule->findById($id);
        $this->set(array(
            'vesselvoyageschedule' => $vesselvoyageschedule,
            '_serialize' => array('vesselvoyageschedule')
        ));
    }
 
     
    public function edit($id) {
        $this->Vesselvoyageschedule->id = $id;
        if ($this->Vesselvoyageschedule->save($this->request->data)) {
            $message = 'Saved';
        } else {
            $message = 'Error';
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
    }
     
    public function delete($id) {
        if ($this->Vesselvoyageschedule->delete($id)) {
            $message = 'Deleted';
        } else {
            $message = 'Error';
        }
        $this->set(array(
            'message' => $message,
            '_serialize' => array('message')
        ));
    }
}