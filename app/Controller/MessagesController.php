<?php
class MessagesController extends AppController{
	public $helpers=array('Html','Form');
    public $components = array('RequestHandler');

	public function index(){
		//$this->set('vessels',$this->Vessel->find('all'));
	}

	// public function sendToIndividual() {
 //        if ($this->request->is('post')) {
 //                 debug($this->request->data);
 //            $this->Message->create();
 //            debug($this->request->data);
 //            if ($this->Message->save($this->request->data)) {
 //                $this->Session->setFlash(__('The recipe has been created'));
 //                //$this->redirect(array('action' => 'index'));
 //                debug($this->request->data);
 //            } else {
 //                $this->Session->setFlash(__('The recipe could not be created. Please, try again.'));
 //            }   
 //        }
 
 // 		$this->loadModel('User');
 //        $users = $this->User->find('list',array('fields' => array('User.username'),'order' => array('User.username ASC')));
 //        $this->set('users',$users); 
 //    }

   //  public function sendToVessel() {
   //      $Message= array();
   //      $temp=array();
   //      $temp1=array();
   //      if ($this->request->is('post')) {
                 
   //          //$Message['Message']['message']=74;
   //          //$Message['Message']['User']=2;
   //          $this->loadModel('Vessel');
   //          $vessel = $this->Vessel->findById($this->request->data['Message']['Vessel']);
   //          debug($vessel);
   //          $Message['Message']['message']=$this->request->data['Message']['message'];
   //          foreach($vessel['User'] as $ids){
   //              array_push($temp,$ids['id']);
   //          }
   //          $temp1['User']=$temp;
   //          $Message['Message']['User']=$temp;
   //          debug($Message);
   //          if ($this->Message->saveall($Message)) {
   //              $this->Session->setFlash(__('The recipe has been created'));
   //              //$this->redirect(array('action' => 'index'));
   //              } else {
   //              $this->Session->setFlash(__('The recipe could not be created. Please, try again.'));
   //              }  
   //      }
        
 		// $this->loadModel('Vessel');
   //      $vessels = $this->Vessel->find('list',array('fields' => array('Vessel.name'),'order' => array('Vessel.name ASC')));
   //      $this->set('vessels',$vessels);
   //      if ($this->request->is('post')){
   //      $vessel = $this->Vessel->findById($this->request->data('Message.Vessel'));
   //      }
   //  }


    // public function sendToVessel() {
    //     $this->loadModel('User');
    //     $this->loadModel('Vessel');
    //     debug($this->Auth->user('id'));
    //     if($this->request->is('post')){
    //         $this->Message->create();
    //         //in $this->request->data array, Vessel is wrong but vessel is correct, remove Vessel replace with vessel
    //         $this->request->data['Message']['vessel_id']=$this->request->data['Message']['Vessel_id'];
    //         unset($this->request->data['Message']['Vessel']);
            


    //         $vessel = $this->Vessel->findById($this->request->data['Message']['vessel_id']);
    //         $i=0;
    //         $tes='';
            
    //         debug($tes);
    //         if($this->Message->save($this->request->data)){
    //             $an = new GCMPushMessage("AIzaSyA0FwIpD_O6aozKY09U8Pv9ijqRqGRo_Vg");
    //             //$an->setDevices("APA91bHvPfdZl-ZXI4WfgKz45iX3HZAoS-mHKgJi6o4WwC7uVBJHOt7et-E4-9ClBD7-CuPqDlUCck6NRgFJJeH0YdbUlqK3rmMtsU8SoWKPElUD0L95KdgWhC1LGHSc4H_xDOIB2bLr03oxKDn4_ZXSNYR2gtWBLg");
    //             foreach($vessel['User'] as $items){
    //                 $an->setDevices($vessel['User'][$i]['registration']);
    //                 $response = $an->send($this->request->data['Message']['message']);
    //                 $i++;
    //             }
                
                
    //             $this->Session->setFlash(_('Your post has been saved.'));
    //             return $this->redirect(array('action'=>'vesselmessagepage'));
    //         }
 
    //         $this->Session->setFlash(_('Unable to add your post'));
    //     }

    //     //get vessels list
    //     $this->loadModel('Vessel');
    //     $vessels = $this->Vessel->find('list',array('fields' => array('Vessel.name'),'order' => array('Vessel.name ASC')));
    //     $this->set('vessels',$vessels);
    // }

	//get messages for all vessels subscribed by user
    public function getMessages_rest($id){
    	$usefulArray=array();

    	$this->loadModel('User');
    	$vessel = $this->User->findById($id);
    	foreach($vessel['Vessel'] as $items){
    		//debug($items['shipping_agent_name']);
    		$return=$this->Message->find('all',array('conditions'=>array('Vessel.shipping_agent_name'=>$items['shipping_agent_name'])));
    		foreach($return as $messages){
    			array_push($usefulArray,$messages);
    		}
    	}
    	//debug($vessel['Vessel']);
    	//$return=$this->Message->find('all');
    	//debug($usefulArray);
        $this->set(array(
            'messages'=>$usefulArray,
            '_serialize' => array('messages')
        ));
    }

	//get messages for all vessels subscribed by user(throught POST)
    public function sendToVessel_rest() {
        $this->loadModel('User');
        $this->loadModel('Vessel');
        $vesselID=$this->Vessel->find('all',array('conditions'=>array('Vessel.name'=>$this->request->data['Message']['Vessel']),'fields'=>array('Vessel.id')));
        //debug($vesselID[0]['Vessel']['id']);

        debug($this->request->data);
        if($this->request->is('post')){
            $this->Message->create();
            $this->request->data['Message']['Vessel']=$vesselID[0]['Vessel']['id'];
            //in $this->request->data array, Vessel is wrong but vessel is correct, remove Vessel replace with vessel
            $this->request->data['Message']['vessel_id']=$this->request->data['Message']['Vessel'];
            unset($this->request->data['Message']['Vessel']);
            


            $vessel = $this->Vessel->findById($this->request->data['Message']['vessel_id']);
            $i=0;
            $tes='';

            if($this->Message->save($this->request->data)){
                $an = new GCMPushMessage("AIzaSyA0FwIpD_O6aozKY09U8Pv9ijqRqGRo_Vg");
                //$an->setDevices("APA91bHvPfdZl-ZXI4WfgKz45iX3HZAoS-mHKgJi6o4WwC7uVBJHOt7et-E4-9ClBD7-CuPqDlUCck6NRgFJJeH0YdbUlqK3rmMtsU8SoWKPElUD0L95KdgWhC1LGHSc4H_xDOIB2bLr03oxKDn4_ZXSNYR2gtWBLg");
                foreach($vessel['User'] as $items){
                    $an->setDevices($vessel['User'][$i]['registration']);
                    $response = $an->send($this->request->data['Message']['message']);
                    $i++;
                }
                
                
                $this->Session->setFlash(_('Your post has been saved.'));
                //return $this->redirect(array('action'=>'vesselmessagepage'));
            }
 
            $this->Session->setFlash(_('Unable to add your post'));
        }

        //get vessels list
        $this->loadModel('Vessel');
        $vessels = $this->Vessel->find('list',array('fields' => array('Vessel.name'),'order' => array('Vessel.name ASC')));
        $this->set('vessels',$vessels);
        $this->set(array(
                'vessels'=>$vessel['User'][0]['registration'],
                '_serialize' => array('vessels')
            ));
    }

    
    // public function vesselMessagePage(){
    //     $this->set('messages',$this->Message->find('all'));
    //     $this->loadModel('Vessel');
    //     $vessels = $this->Vessel->find('list',array('fields' => array('Vessel.name')));
    //     $this->set('vessels',$vessels);
    // }
	
    // public function singleMessagePage(){
    //     $this->set('messages',$this->Message->find('all'));
    // }
}



//class to push message to android
class GCMPushMessage {
    var $url = 'https://android.googleapis.com/gcm/send';
    var $serverApiKey = "";
    var $devices = array();
    
    /*
        Constructor
        @param $apiKeyIn the server API key
    */
    function GCMPushMessage($apiKeyIn){
        $this->serverApiKey = $apiKeyIn;
    }
    /*
        Set the devices to send to
        @param $deviceIds array of device tokens to send to
    */
    function setDevices($deviceIds){
    
        if(is_array($deviceIds)){
            $this->devices = $deviceIds;
        } else {
            $this->devices = array($deviceIds);
        }
    
    }
    /*
        Send the message to the device
        @param $message The message to send
        @param $data Array of data to accompany the message
    */
    function send($message, $data = false){
        
        if(!is_array($this->devices) || count($this->devices) == 0){
            $this->error("No devices set");
        }
        
        if(strlen($this->serverApiKey) < 8){
            $this->error("Server API Key not set");
        }
        
        $fields = array(
            'registration_ids'  => $this->devices,
            'data'              => array( "message" => $message ),
        );
        
        if(is_array($data)){
            foreach ($data as $key => $value) {
                $fields['data'][$key] = $value;
            }
        }
        $headers = array( 
            'Authorization: key=' . $this->serverApiKey,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
        
        // Set the url, number of POST vars, POST data
        curl_setopt( $ch, CURLOPT_URL, $this->url );
        
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
        
        // Avoids problem with https certificate
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
        
        // Execute post
        $result = curl_exec($ch);
        
        // Close connection
        curl_close($ch);
        
        return $result;
    }
    
    function error($msg){
        echo "Android send notification failed with error:";
        echo "\t" . $msg;
        exit(1);
    }
}

