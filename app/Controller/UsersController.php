<?php

class UsersController extends AppController{
	public $helpers=array('Html','Form');
	public $components = array('RequestHandler');
	
	//page for super admin to view all user details
	public function index(){
		$this->set('users',$this->User->find('all',array('order' => array('User.username ASC'))));
	}

	//page for admin to add messenger
	public function indexAddMessenger(){
		//debug($this->Session->read('Auth.User.id'));
		$this->set('users',$this->User->find('all',array('order' => array('User.username ASC'),'conditions'=>array('User.is_admin'=>1,'User.master'=>$this->Session->read('Auth.User.id')))));
	}


	// public function add(){
	// 	debug($this->Auth->user('id'));
	// 	if($this->request->is('post')){
	// 		$this->User->create();
	// 		if($this->User->save($this->request->data, array('validate' => true))){
	// 			$this->Session->setFlash(_('Your post has been saved.'));
	// 			return $this->redirect(array('action'=>'index'));
	// 		}
	// 		//debug($this->User->validationErrors['username'][0]);
	// 		$this->Session->setFlash(_('Unable to add your post'));
	// 	}
	// } 

	//add super admin or admin
	public function addSuper(){
		
		if($this->request->is('post')){
			$this->User->create();
			if($this->User->save($this->request->data, array('validate' => true))){
				$this->Session->setFlash(_('Your post has been saved.'));
				return $this->redirect(array('action'=>'index'));
			}
			//debug($this->User->validationErrors['username'][0]);
			$this->Session->setFlash(_('Unable to add'));
		}
	}

	//get the list of vessels that only a particular group of messengers can access(through POST) 
	public function getListMessenger_rest($id=null){
			$this->loadModel('Vessel');	
			$agentName=$this->User->find('all',array('conditions'=>array('User.id'=>$id),'fields'=>array('User.agent_name')));
			//debug($this->Session->read('Auth.User.id'));
			//debug($agentName[0]['User']['agent_name']);
			$vessel=$this->Vessel->find('all',array('conditions'=>array('Vessel.shipping_agent_name'=>$agentName[0]['User']['agent_name'])));
			//debug($vessel);
			$idArray=array();
			$nameArray=array();
			foreach($vessel as $vesselElements){
				$vesselElements['Vessel']['id'];
				array_push($idArray,$vesselElements['Vessel']['id']);
				array_push($nameArray,$vesselElements['Vessel']['name']);
			}
			//debug($idArray);
			//debug($nameArray);

			$this->set(array(
	 				'id'=>$idArray,
					'vesselname'=>$nameArray,
	            	'_serialize' => array('id','vesselname')
	        	));
	}

	//add messenger
	public function addMessenger(){		
		if($this->request->is('post')){
			$this->User->create();
			$this->request->data['User']['master']=$this->Session->read('Auth.User.id');
			$this->request->data['User']['agent_name']=$this->Auth->user('agent_name');
			if($this->User->save($this->request->data, array('validate' => true))){
				$this->Session->setFlash(_('Your post has been saved.'));
				return $this->redirect(array('action'=>'indexaddmessenger'));
			}
			//debug($this->User->validationErrors['username'][0]);
			$this->Session->setFlash(_('Unable to add your post'));
		}
	}

	//normal user signup for account(through POST)
	public function add_rest(){
		if($this->request->is('post')){
			$this->User->create();
			if($this->User->save($this->request->data, array('validate' => true))){
				$this->set(array(
					'error1'=>'null',
					'error2'=>'null',
 					'message'=>'success',
            		'_serialize' => array('error1','error2','message')
        	));
        	}else{
				if(!isset($this->User->validationErrors['username'][0])){
					$error1="null";
				}else{
					$error1=$this->User->validationErrors['username'][0];
				}

				if(!isset($this->User->validationErrors['email'][0])){
					$error2="null";
				}else{
					$error2=$this->User->validationErrors['email'][0];
				}
				$this->set(array(
	 				'error1'=>$error1,
					'error2'=>$error2,
					'message'=>'fail',
	            	'_serialize' => array('error1','error2','message')
	        	));}
		}
	} 

	//login for super admin and admin
	public function login() {
         
        //if already logged-in, redirect
        if($this->Session->check('Auth.User')){

			if($this->Auth->user('is_admin')==3){

            	$this->redirect($this->Auth->redirectUrl('index'));
            }else if($this->Auth->user('is_admin')==2){
            	$this->Session->setFlash(__('Welcome, '. $this->Auth->user('username')));
            	debug($this->request->data);
            	$this->redirect($this->Auth->redirectUrl('/users/indexaddmessenger'));
            }else{
        		$this->Auth->logout();
        		$this->Session->setFlash(__('Only admin is allowed to login!!'));    
        	}
        }
         
        // if we get the post information, try to authenticate
        if ($this->request->is('post')) {
	        if ($this->Auth->login()) {
	        	if($this->Auth->user('is_admin')==3){
	            	$this->Session->setFlash(__('Welcome, '. $this->Auth->user('username')));
	            	debug($this->request->data);
	            	$this->redirect($this->Auth->redirectUrl('index'));
	            }else if($this->Auth->user('is_admin')==2){
	            	$this->Session->setFlash(__('Welcome, '. $this->Auth->user('username')));
	            	debug($this->request->data);
	            	$this->redirect($this->Auth->redirectUrl('/users/indexaddmessenger'));
	            }else{
	        		$this->Auth->logout();
	        		$this->Session->setFlash(__('Only admin is allowed to login!!'));
	        	}
	        } else {
	            $this->Session->setFlash(__('Invalid username or password'));
	        }     
        } 
    }

    //login for normal user and messenger
    public function login_rest(){
    	

    	$ids="null";
    	//due to double single quotes, get first array
    	foreach($this->request->data as $arr){
    		$arrs=$arr;
    	}

    	//get first array again
    	foreach($arrs as $arrss){
    		$arrsss=$arrss;
    	}
    	
    	//$arrsss is the registration id
    	//debug($arrsss);
    	// if we get the post information, try to authenticate
        if ($this->request->is('post')) {
	        if ($this->Auth->login()) {
	        	$this->User->id = $this->Session->read('Auth.User.id');
				$this->User->saveField('registration', $arrsss);
	        	if($this->Auth->user('is_admin')==1){
		        	$success = "true";
		        	$is_admin = "true";
	        	}else{
	        		$success = "true";
	        		$is_admin = "false";
	        	}
	        } else {
	            $success = "false";
	        	$is_admin = "null";
	        }     
        } 
        $ids=$this->Session->read('Auth.User.id');
        
        $this->set(array(
 			'id'=>$ids ,
            'success' => $success,
            'is_admin' => $this->Auth->user('is_admin'),
            '_serialize' => array('id','success','is_admin')
        ));
    }
 
    public function logout() {
        $this->redirect($this->Auth->logout());
    }

    public function logout_rest() {
        $this->Auth->logout();

        $this->set(array(
            'success' => 'yes',
            '_serialize' => array('success')
        ));
    }

    //super admin edit
	public function edit($id=null){
		/* ensure user access an existing record by using id check
		   $id=null above set $id to null by default
		   $id is still null if no id is passed in
		*/
		if(!$id){
			throw new NotFoundException(_('Invalid post'));
		}

		$post=$this->User->findById($id);

		//ensure user arrive with post
		if(!$post){
			throw new NotFoundException(_('Invalid post'));
		}

		if($this->request->is(array('post','put'))){
			$this->User->id=$id;
			if($this->User->save($this->request->data)){
				$this->Session->setFlash(_('Your post has been updated.'));
				return $this->redirect(array('action'=>'index'));
			}
			$this->Session->setFlash(_('Unable to update your post.'));
		}

		if(!$this->request->data){
			$this->request->data=$post;
		}
	}

	public function delete($id){
		if($this->request->is('get')){
			throw new MethodNotAllowedException();
		}

		if($this->User->delete($id)){
			$this->Session->setFlash(_('The user with id:'.$id.' has been deleted.'));
			return $this->redirect(array('action'=>'index'));
		}
	}

	//get list of user subscribed vessels
	function getSubscribedList_rest($id){
		$vessel = $this->User->findById($id);
		//debug($vessel['Vessel']);
		$this->set(array(
 				'vessel'=>$vessel['Vessel'],
            	'_serialize' => array('vessel')
        	));
	}
}

