<?php
class VesselsController extends AppController{
	public $helpers=array('Html','Form');
	public $components = array('RequestHandler');

	//admin view all vessels
	public function index(){
		$this->set('vessels',$this->Vessel->find('all',array('order' => array('Vessel.name ASC'))));
	}

	// public function add() {
 //        if ($this->request->is('post')) {
		
                 
 //            $this->Vessel->create();

 //            if ($this->Vessel->save($this->request->data)) {
 //                $this->Session->setFlash(__('The vessel has been created'));
 //                debug($this->request->data);
 //                //$this->redirect(array('action' => 'index'));
 //            } else {
 //                $this->Session->setFlash(__('The recipe could not be created. Please, try again.'));
 //            }  
 //        }
 
 // 		$this->loadModel('User');
 //        $users = $this->User->find('list',array(
 //        'fields' => array('User.username'),'order' => array('User.username ASC')
 //    	));
 //        $this->set('users',$users);
 //    }

//admin add vessel
	public function add() {
        if ($this->request->is('post')) {
  
            $this->Vessel->create();

            if ($this->Vessel->save($this->request->data)) {
                $this->Session->setFlash(__('The vessel has been created'));
                debug($this->request->data);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The vessel could not be added. Please, try again.'));
            }  
        }
    }

	// public function edit($id) {
 //        if (!$id) {
	// 		$this->Session->setFlash('Please provide a vessel id');
	// 		$this->redirect(array('action'=>'index'));
	// 	}

	// 	$vessel = $this->Vessel->findById($id);
	// 	debug($vessel);
	// 	if (!$vessel) {
	// 		$this->Session->setFlash('Invalid Vessel ID Provided');
	// 		$this->redirect(array('action'=>'index'));
	// 	}
		
	// 	if ($this->request->is('post') || $this->request->is('put')) {
	// 		$this->Vessel->id = $id;
	// 		if ($this->Vessel->save($this->request->data)) {
	// 			$this->Session->setFlash(__('The vessel has been updated'));
	// 			$this->redirect(array('action' => 'edit',$id));
	// 		}else{
	// 			$this->Session->setFlash(__('Unable to update your vessel.'));
	// 		}
	// 	}

	// 	if (!$this->request->data) {
	// 		$this->request->data = $vessel;
			
	// 	}
	// 	$this->loadModel('User');	
 //        $users = $this->User->find('list',array(
 //        'fields' => array('User.username'),'order' => array('User.username ASC')
 //    ));
 //        debug($users);
 //        $this->set('users',$users);
 //    }

    //admin edit vessel
    public function edit($id=null){
		/* ensure user access an existing record by using id check
		   $id=null above set $id to null by default
		   $id is still null if no id is passed in
		*/
		if(!$id){
			throw new NotFoundException(_('Invalid vessel'));
		}

		$post=$this->Vessel->findById($id);

		//ensure user arrive with post
		if(!$post){
			throw new NotFoundException(_('Invalid post'));
		}

		if($this->request->is(array('post','put'))){
			$this->Vessel->id=$id;
			if($this->Vessel->save($this->request->data)){
				$this->Session->setFlash(_('Your post has been updated.'));
				return $this->redirect(array('action'=>'index'));
			}
			$this->Session->setFlash(_('Unable to update your post.'));
		}

		if(!$this->request->data){
			$this->request->data=$post;
		}
	}

	//users subscribe to particular vessel
    public function subscribe_user_rest() {

    	/*get vessel ID*/
    	//return id=scn but key is id
		$vesselID = $this->Vessel->find('list', array(
    		'conditions'=>array('Vessel.name' => $this->request->data['Vessel']['name'])));

		//extract the key
		reset($vesselID);
		$first_key=key($vesselID);

		//find by id return alot of stuff including vessel id,scn and name AND all info of user of the vessel 
		$vessel = $this->Vessel->findById($first_key);


		$i=0;
		
		
		//data[Vessel][id]=11
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Vessel->id = $first_key;
			$this->request->data['Vessel']['id']=$first_key;
			
			debug($this->request->data);
			//add all the old users to the new input to solve cakephp problem, cake php will erase old data and 
			//put in new data
			foreach($vessel['User'] as $item){
				array_push($this->request->data['Vessel']['User'],$vessel['User'][$i]['id']);
				$i++;
			}
			debug($this->request->data);

			if ($this->Vessel->saveall($this->request->data)) {

			}else{
				$this->Session->setFlash(__('Unable to update your vessel.'));
			}
		}

		if (!$this->request->data) {
			$this->request->data = $vessel;
			
		}
		$this->loadModel('User');	

        $this->set(array(
 				'message'=>'subscribed',
            	'_serialize' => array('message')
        	));
    }

    //user unsubscribe a vessel
    public function unsubscribe_user_rest() {
    	$removeItem="";
    	/*get vessel ID*/
    	//return id=scn but key is id
		$vesselID = $this->Vessel->find('list', array(
    		'conditions'=>array('Vessel.name' => $this->request->data['Vessel']['name'])));

		//extract the key
		reset($vesselID);
		$first_key=key($vesselID);

		//find by id return alot of stuff including vessel id,scn and name AND all info of user of the vessel 
		$vessel = $this->Vessel->findById($first_key);
		debug($vessel);

		$i=0;
		
		
		//data[Vessel][id]=11
		if ($this->request->is('post')) {
			$this->Vessel->id = $first_key;
			$this->request->data['Vessel']['id']=$first_key;
			
			debug($this->request->data);
			//add all the old users to the new input to solve cakephp problem, cake php will erase old data and 
			//put in new data
			$removeItem=$this->request->data['Vessel']['User'][0];
			array_pop($this->request->data['Vessel']['User']);
			foreach($vessel['User'] as $item){
				if(strcmp($vessel['User'][$i]['id'],$removeItem)==0){

				}else{
					array_push($this->request->data['Vessel']['User'],$vessel['User'][$i]['id']);
				}
				
				$i++;
			}
			debug($this->request->data);

			if ($this->Vessel->saveall($this->request->data)) {

			}else{
				$this->Session->setFlash(__('Unable to update your vessel.'));
			}
		}

		if (!$this->request->data) {
			$this->request->data = $vessel;
			
		}
		$this->loadModel('User');	

        $this->set(array(
 				'message'=>'subscribed',
            	'_serialize' => array('message')
        	));
    }


 //    function getVessels_rest(){
 //    	//$this->Vessel->find('all',array('order' => array('Vessel.name ASC'));

	// 	$this->set(array(
	// 			'vessels'=>$this->Vessel->find('all',array('order' => array('Vessel.name ASC'),'fields'=>array('Vessel.name'))),
 //        	'_serialize' => array('vessels')
 //    	));
	// }

    //
	// function getSubscribedList_rest(){
	// 	$vessel = $this->Vessel->findById('24');
	// 	debug($vessel);
	// 	$this->set(array(
 // 				'message'=>'subscribed',
 //            	'_serialize' => array('message')
 //        	));
	// }

    //remove the vessel
    function delete($id) {
        $this->Vessel->id = $id;
        $this->Vessel->delete();
		$this->Session->setFlash('Recipe has been deleted.');
        $this->redirect(array('controller'=>'Vessels','action'=>'index'));
    }
}