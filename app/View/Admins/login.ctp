<div class="admins form">
<?php echo $this->Session->flash('auth'); ?>
<?php echo $this->Form->create('Admin'); ?>
    <fieldset>
        <legend><?php echo __('Please enter your name and password'); ?></legend>
        <?php echo $this->Form->input('name');
        echo $this->Form->input('password');
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Login')); ?>
</div>
