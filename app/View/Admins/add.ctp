<!-- app/View/Users/add.ctp -->
<div class="admins form">
 
<?php echo $this->Form->create('Admin');?>
    <fieldset>
        <legend><?php echo __('Add Admin'); ?></legend>
        <?php echo $this->Form->input('name');
        echo $this->Form->input('email');
        echo $this->Form->input('password');
        echo $this->Form->input('password_confirm', array('label' => 'Confirm Password *', 'maxLength' => 255, 'title' => 'Confirm password', 'type'=>'password'));
        echo $this->Form->submit('Add Admin', array('class' => 'form-submit',  'title' => 'Click here to add the admin') ); 
?>
    </fieldset>
<?php echo $this->Form->end(); ?>
</div>
