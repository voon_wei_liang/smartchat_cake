<h1>Groups</h1>
<p><?php echo $this->Html->link("Add Group",array('action'=>'add')); ?></p>
<table class="table table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>Message</th>
			<th>Date and Time</th>
			<th>Sender</th>
		</tr>
	</thead>
	<tbody>
		<!--loop through-->
		<?php foreach ($messages as $message): ?>
		<tr>
			<td><?php echo $message['Message']['id']; ?></td>
			<td><?php echo $message['Message']['message']; ?></td>
			<td><?php echo $message['Message']['date_time']; ?></td>
			<td><?php echo $message['Message']['sender']; ?></td>
		</tr>
		<?php endforeach; ?>
		<?php unset($post); ?>
	</tbody>

</table>