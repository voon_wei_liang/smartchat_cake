<div class="groups form">
<?php echo $this->Form->create('Group', array(
'class' => 'form-horizontal', 
'role' => 'form',
'inputDefaults' => array(
    'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
    'div' => array('class' => 'form-group'),
    'class' => array('form-control'),
    'label' => array('class' => 'col-lg-1 control-label'),
    'between' => '<div class="col-lg-3">',
    'after' => '</div>',
    'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
))); ?>
    <fieldset>
        <legend><?php echo __('Edit Group'); ?></legend>
        <?php 
		echo $this->Form->hidden('id', array('value' => $this->data['Vessel']['id']));
		echo $this->Form->input('Vessel.name');
		echo $this->Form->input('Vessel.shipping_agent_name');
        echo $this->Form->end(array(
                'label' => __('Submit'),
                'class' => 'btn btn-primary col-md-offset-2',
                'div' => array(
                    'class' => 'control-group',
                    ),
                'before' => '<div class="controls">',
                'after' => '</div>'
            ));

		//echo $this->Form->submit('Edit Group', array('class' => 'form-submit',  'title' => 'Click here to add the recipe') ); 
?>
    </fieldset>
<?php echo $this->Form->end(); ?>
</div>
