<!-- app/View/Recipes/add.ctp -->
 <h1>Add Vessel</h1>
<div class="users form">

<?php //echo "asdasd".AuthComponent::user('id');
echo $this->Form->create('Vessel', array(
'class' => 'form-horizontal', 
'role' => 'form',
'inputDefaults' => array(
    'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
    'div' => array('class' => 'form-group'),
    'class' => 'form-control',
    'label' => array('class' => 'col-lg-1 control-label'),
    'between' => '<div class="col-lg-3">',
    'after' => '</div>',
    'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
)));?>
    <fieldset>
        
        <?php
    		echo $this->Form->input('name');
    		echo $this->Form->input('shipping_agent_name');
    		//echo $this->Form->submit('Add Vessel', array('class' => 'form-submit',  'title' => 'Click here to add the group') );
            echo $this->Form->end(array(
                'label' => __('Submit'),
                'class' => 'btn btn-primary col-md-offset-2',
                'div' => array(
                    'class' => 'control-group',
                    ),
                'before' => '<div class="controls">',
                'after' => '</div>'
            ));
        ?>
    </fieldset>
<?php  ?>
</div>
