<h1>Vessels</h1>
<a href="./vessels/add" class="btn btn-primary" role="button">Add Vessel</a>
</br>
</br>
<table class="table table-striped">
	<thead>
		<tr>
			<th>ID</th>		
			<th>Name</th>
			<th>Belongs To</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<!--loop through-->
		<?php foreach ($vessels as $vessel): ?>
		<tr>
			<td><?php echo $vessel['Vessel']['id']; ?></td>			
			<td><?php echo $vessel['Vessel']['name']; ?></td>
			<td><?php echo $vessel['Vessel']['shipping_agent_name']; ?></td>
			<td><?php echo $this->Html->link('Edit',array('action'=>'edit',$vessel['Vessel']['id'])); ?>
				<?php echo ' | '.$this->Form->postLink('Delete',array('action'=>'delete', $vessel['Vessel']['id']),array('confirm'=>'Are you sure?')); ?></td>
		</tr>
		<?php endforeach; ?>
		<?php unset($post); ?>
	</tbody>

</table>