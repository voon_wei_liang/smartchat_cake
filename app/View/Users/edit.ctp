<h1>Edit User</h1>
<?php
echo $this->Form->create('User', array(
'class' => 'form-horizontal', 
'role' => 'form',
'inputDefaults' => array(
    'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
    'div' => array('class' => 'form-group'),
    'class' => array('form-control'),
    'label' => array('class' => 'col-lg-1 control-label'),
    'between' => '<div class="col-lg-3">',
    'after' => '</div>',
    'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
)));
echo $this->Form->input('username');
echo $this->Form->input('email');
echo $this->Form->input('User.registration', array(
    'label' => array(
        'class' => 'col-lg-1 control-label',
        'text' => 'Registration ID'
    )
));
echo $this->Form->input('is_admin');
echo $this->Form->input('master');

//if id is not present, cakephp assume you are inserting new model
echo $this->Form->input('id',array('type'=>'hidden'));
echo $this->Form->end(array(
                'label' => __('Submit'),
                'class' => 'btn btn-primary col-md-offset-2',
                'div' => array(
                    'class' => 'control-group',
                    ),
                'before' => '<div class="controls">',
                'after' => '</div>'
            ));
?>