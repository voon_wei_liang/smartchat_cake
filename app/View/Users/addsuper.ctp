 <h1>Add Super Admin or Admin</h1>
<?php
echo $this->Form->create('User', array(
'class' => 'form-horizontal', 
'role' => 'form',
'inputDefaults' => array(
    'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
    'div' => array('class' => 'form-group'),
    'class' => 'form-control',
    'label' => array('class' => 'col-lg-1 control-label'),
    'between' => '<div class="col-lg-3">',
    'after' => '</div>',
    'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
)));
echo $this->Form->input('username');
echo $this->Form->input('email');
echo $this->Form->input('password');

echo $this->Form->input('password_confirm', array('label' => array('for'=>'Confirm Password *','class'=>'col-lg-1 control-label'), 'maxLength' => 255, 'title' => 'Confirm password', 'type'=>'password'));
echo '<div class="form-group">
			<label for="is_admin" class="control-label col-lg-1">Role</label>
				<div class="radio col-lg-1">
					<label>
					<input type="radio" name="data[User][is_admin]" id="UserIsAdmin2"  value="2" checked="checked" />Admin
					</label>
				</div>
				<div class="radio col-lg-1">
					<label>
					<input type="radio" name="data[User][is_admin]" id="UserIsAdmin3"  value="3" />
					Super Admin
					</label>
				</div>
	</div>';

echo $this->Form->input('User.agent_name', array(
    'label' => array(
        'class' => 'col-lg-1 control-label',
        'text' => 'Agent Name(fill this if Admin is selected)'
    )
));

echo $this->Form->end(array(
    'label' => __('Submit'),
    'class' => 'btn btn-primary col-md-offset-2',
    'div' => array(
        'class' => 'control-group',
        ),
    'before' => '<div class="controls">',
    'after' => '</div>'
));
?>