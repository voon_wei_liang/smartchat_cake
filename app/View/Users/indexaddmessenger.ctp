 <h1>Messengers</h1>
<p><?php echo $this->Html->link("Add Messenger",array('action'=>'addmessenger')); ?></p>
<table class="table table-striped">
	<thead>
		<tr>
			<th>ID</th>
			<th>User Name</th>
			<th>Email</th>
			<th>Registration ID</th>
			<th>is_admin</th>
			<th>master</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<!--loop through-->
		<?php foreach ($users as $user): ?>
		<tr>
			<td><?php echo $user['User']['id']; ?></td>
			<td><?php echo $user['User']['username']; ?></td>
			<td><?php echo $user['User']['email']; ?></td>
			<td><?php echo $user['User']['registration']; ?></td>
			<td><?php echo $user['User']['is_admin']; ?></td>
			<td><?php echo $user['User']['master']; ?></td>
			<td><?php echo $this->Html->link('Edit',array('action'=>'edit',$user['User']['id'])); ?>
				<?php echo ' | '.$this->Form->postLink('Delete',array('action'=>'delete', $user['User']['id']),array('confirm'=>'Are you sure?')); ?></td>
		</tr>
		<?php endforeach; ?>
		<?php unset($post); ?>
	</tbody>
</table>