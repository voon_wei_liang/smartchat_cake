<?php

class Message extends AppModel{
	var $name='Message';

	// var $hasAndBelongsToMany=array(
	// 	'User'=>array(
	// 		'className'=>'User',
	// 		'joinTable'=>'messages_users',
	// 		'foreignKey'=>'message_id',
	// 		'associationForeignKey'=>'user_id'
	// 	),
	// 	'Vessel'=>array(
	// 		'className'=>'Vessel',
	// 		'joinTable'=>'vessels_messages',
	// 		'foreignKey'=>'message_id',
	// 		'associationForeignKey'=>'vessel_id'
	// 	),
	// );

	// this is important for the correct left joins
var $belongsTo = array(
    'Vessel' => array(
        'className' => 'Vessel',
        'foreignKey' => 'vessel_id'
    ),
    'User' => array(
        'className' => 'User',
        'foreignKey' => 'sender_id'
    )
);





	public function beforeSave($options = array()) {
 
        // save our HABTM relationships
        foreach (array_keys($this->hasAndBelongsToMany) as $model){
                if(isset($this->data[$this->name][$model])){
                        $this->data[$model][$model] = $this->data[$this->name][$model];
                        unset($this->data[$this->name][$model]);
                }
        }
    }
}
