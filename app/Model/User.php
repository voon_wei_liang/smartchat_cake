<?php

class User extends AppModel{
	var $name='User';

    public $validate = array(
        'username' => array(
            'rule' => 'usernameUnique',
            'message' => "Name already exist"
        ),
        'email' => array(
            'rule' => 'emailUnique',
            'message' => "Email already exist"
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Password is required.'
            )
        )
    );

    public function usernameUnique() 
    {
        $existing = $this->find('first', array(
            'conditions' => array(
                'username' => $this->data[$this->name]['username'],
             )
        ));

        return (count($existing) == 0);
    }

    public function emailUnique() 
    {
        $existing = $this->find('first', array(
            'conditions' => array(
                'email' => $this->data[$this->name]['email']
             )
        ));

        return (count($existing) == 0);
    }

	var $hasAndBelongsToMany=array(
		// 'Message'=>array(
		// 	'className'=>'Message',
		// 	'joinTable'=>'messages_users',
		// 	'foreignKey'=>'user_id',
		// 	'associationForeignKey'=>'message_id'
		// ),
		'Vessel'=>array(
			'className'=>'Vessel',
			'joinTable'=>'subscriptions',
			'foreignKey'=>'user_id',
			'associationForeignKey'=>'vessel_id'
		)
	);

    // for has many this is usually not necessary/possible (1:n), you can try though
var $hasMany = array(
    'Message' => array(
        'className' => 'Message',
        'foreignKey' => false
    )
); 


	public function beforeSave($options = array()) {
 
		// hash our password
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
         
        // if we get a new password, hash it
        if (isset($this->data[$this->alias]['password_update']) && !empty($this->data[$this->alias]['password_update'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password_update']);
        }
     
        // fallback to our parent
        return parent::beforeSave($options);

        // save our HABTM relationships
        foreach (array_keys($this->hasAndBelongsToMany) as $model){
                if(isset($this->data[$this->name][$model])){
                        $this->data[$model][$model] = $this->data[$this->name][$model];
                        unset($this->data[$this->name][$model]);
                }
        }

        // fallback to our parent
        return parent::beforeSave($options);
    }
}
