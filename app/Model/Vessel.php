<?php

class Vessel extends AppModel{
	var $name='Vessel';

	public $validate = array(
        'name' => array(
            'rule' => 'nameUnique',
            'message' => "vessel already exist"
        )
    );

	var $hasAndBelongsToMany=array(
		'User'=>array(
			'className'=>'User',
			'joinTable'=>'subscriptions',
			'foreignKey'=>'vessel_id',
			'associationForeignKey'=>'user_id'
		),
	);
	// for has many this is usually not necessary/possible (1:n), you can try though
	var $hasMany = array(
	    'Message' => array(
	        'className' => 'Message',
	        'foreignKey' => false
	    )
	); 


	public function beforeSave($options = array()) {
 
        // save our HABTM relationships
        foreach (array_keys($this->hasAndBelongsToMany) as $model){
                if(isset($this->data[$this->name][$model])){
                        $this->data[$model][$model] = $this->data[$this->name][$model];
                        unset($this->data[$this->name][$model]);
                }
        }
    }
}
