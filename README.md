<<<<<<< HEAD
# Final Year Project: Senari Vessel Application(PHP server) #
### Language used: PHP(web and REST API), Java(Android) ###
### Frameworks: Bootstrap,CakePHP ###
### Database: Mysql ###
### Push Notification: Google GCM ###
### User Manual: User Manual(Web).docx ###
### Live site : www.smartchats.esy.es ###

## An app used by vessel owners: ##
1. view vessel schedule.
2. to be notified about the reason of late departure from port and take immediate action to reduce cost(for example: the topside of the vessel is broken and the containers cannot be moved from the vessel to the port, vessel parking at the port is like airplane parked at airport, they pay parking fee by hour, to reduce the cost, vessel owner send some technician to repair the vessel immediately after being notified so that vessel can be repaired as soon as possible to reduce parking fee).
3. allow workers working at the port to notify workers at the office when assistance is needed.

## A app used by other stakeholders: ##
1. to view vessel schedule.
2. view notifications
